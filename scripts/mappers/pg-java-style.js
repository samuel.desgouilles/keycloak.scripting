// #### Java from Nashorn  ####

var javaImports = new JavaImporter(java.io,java.lang, java.sql , java.util, org.postgresql);

var returnValue = 'default from code';

with (javaImports) {
    
    
    // var jdbc_props_filename = "conf/postgresql.properties";
    // if (arguments[0] != null) {
        //     jdbc_props_filename = arguments[0];
        // } 
        
        var conn = null;
        var exitCode = 1;
        var except = null;
        var inputStream = null;
    try {

        var jdbc_props = new Properties();
        jdbc_props.setProperty("jdbc.url", "jdbc:postgresql://localhost/dbadmin");
        jdbc_props.setProperty("jdbc.username", "dbadmin");
        jdbc_props.setProperty("jdbc.password", "dbadmin");
        // inputStream = new FileInputStream(jdbc_props_filename);
        // jdbc_props.load(inputStream);
        conn = DriverManager.getConnection(
                    jdbc_props.getProperty("jdbc.url"), 
                    jdbc_props.getProperty("jdbc.username"),
                    jdbc_props.getProperty("jdbc.password"));

        var meta = conn.getMetaData();
        print("Connected to: ");
        println(meta.getDatabaseProductName() + " " +
                    meta.getDatabaseProductVersion());
        print("         URL: ");
        println(meta.getURL());
        print("    Username: ");
        println(meta.getUserName());
        print(" JDBC Driver: ");
        println(meta.getDriverName() + " " + meta.getDriverVersion());

//#########   !!!!!!
        var idLookup = 1;
        var st = conn.prepareStatement("SELECT value FROM examples WHERE id = ?");
        st.setInt(1, idLookup);

        var rs = st.executeQuery();
        while (rs.next()) {
            System.out.print("Column 1 returned ");
            // System.out.println(rs.getString(1));
            returnValue = rs.getString(1)
        }
        rs.close();
        st.close();

    } catch (e) {        
        print('catch : ' + e) ;
        java.lang.System.err.println(e);
    } finally {

        print('finally is ' + returnValue) ;
        
        if (except != null) {

            if (except instanceof IOException) {

                println(except.getMessage());
                exitCode = 2;

            } else if (except instanceof SQLException) {

                for (var iter = except.iterator(); iter.hasNext();) {
                    var e = iter.next();
                    println(e.getMessage());
                    if (e instanceof SQLException) 
                        println("SQLSTATE: " + except.getSQLState());
                }
                
                exitCode = 1;
            }

        } else if (conn != null) {

            conn.close();
            exitCode = 0;            

        }

    }
}

print('return value is ' + returnValue) ;
java.lang.System.out.println(returnValue);

returnValue;