DOC
===
https://www.keycloak.org/archive/documentation-11.0.html

==
working dir = scripts
zip MesScripts.jar mappers/*.*js META-INF/*

working dir = racine projet
podman run -dt -p 8080:8080 -p 9990:9990 \
  -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin \
  -e NODE_PATH=/opt/jboss/keycloak/js-attr/js-modules \
  -v ./configuration/profile.properties:/opt/jboss/keycloak/standalone/configuration/profile.properties:Z \
  -v ./scripts/js-modules:/opt/jboss/keycloak/js-attr/js-modules:Z \
  -v ./scripts/MesScripts.jar:/opt/jboss/keycloak/standalone/deployments/MesScripts.jar:Z \
  -v /var/home/sam/Work/keycloak-graalvm-js-scripting-provider/graalvm-js-scripting-provider-bundle/target/graalvm-js-scripting-provider-bundle-1.0-SNAPSHOT.ear:/opt/jboss/keycloak/standalone/deployments/graalvm-js-scripting-provider-bundle-1.0-SNAPSHOT.ear:Z \
  --name keycloack \
  quay.io/keycloak/keycloak:11.0.2

  -v ./scripts/java-libs:/opt/jboss/js-attr/java-libs:Z \

NODE_PATH = chemin de stockage des modules variable pour script jvm-npm
volumes :
 - activation scripting
 - deploiement ear pour environment JS de graalvm 
 - pour les modules JS ( bundle / et npm? )
 - volume = deploiemnt scripts mappers en mode auto ( vs module ?)

 - deploiement jar dependencies (? vs module.path -mp ) 


===

Create object 

    cd $JBOSS_HOME  = /opt/jboss/keycloak

    bin/kcadm.sh create realms -f - << EOF
    { "realm": "demorealm", "enabled": true }
    EOF

    bin/kcadm.sh create clients -r demorealm -s clientId=myapp -s enabled=true

    bin/kcadm.sh create users -r demorealm -s username=testuser -s enabled=true

===
