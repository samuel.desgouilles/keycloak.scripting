 podman run --name pgdatabase \
   -e POSTGRES_USER=dbadmin -e POSTGRES_PASSWORD=dbadmin \
   -p 5432:5432 \
   -d docker.io/postgres:13-alpine

psql -U dbadmin

CREATE TABLE examples(id INTEGER PRIMARY KEY, value VARCHAR);
\dt
INSERT INTO examples values (1,'value to be tested');
select * from examples;


-----

https://www.keycloak.org/docs/11.0/server_installation/index.html#package-the-jdbc-driver


/opt/jboss/keycloak/modules/system/layers/
=> keycloack

base/org/postgresql/jdbc/main


===
PB dependances java !

/subsystem=ee/global-directory=js-attr-libs:add(path=/opt/jboss/js-attr/libs)



 curl -L https://jdbc.postgresql.org/download/postgresql-42.2.18.jar -o /opt/postgresql-42.2.18.jar


changing the configuration by issuing commands via the jboss-cli tool :

    /opt/jboss/keycloak/bin/jboss-cli.sh --command="module add --name=org.postgresql --resources=/opt/jboss/postgresql-42.2.18.jar --dependencies=javax.api,javax.transaction.api"

    /opt/jboss/keycloak/bin/jboss-cli.sh
    connect
    module add --module-root-dir=/opt/jboss/keycloack/modules/ --name=org.postgresql --resources=/opt/jboss/postgresql-42.2.18.jar --dependencies=javax.api,javax.transaction.api


    When a module is added, the corresponding to the module name directory
    structure will be created in the module repository. The JAR files
    specified as resources will be copied to the module's directory,
    the JAR files specified as absolute resources will be referenced
    with absolute paths. Unless module.xml file was specified as an argument,
    it will be automatically generated.


OR
manually

    create folder
    KEYCLOAK_HOME/modules/org/.../.../main. (?../provider/main)  <= main is for version
    Then copy jar
    create module.xml with the following content:

    <?xml version="1.0" encoding="UTF-8"?>
    <module xmlns="urn:jboss:module:1.3" name="org.acme.provider">
        <resources>
            <resource-root path="provider.jar"/>
        </resources>
        <dependencies>
        ...
            <module name="org.keycloak.keycloak-core"/>
            <module name="org.keycloak.keycloak-server-spi"/>
        </dependencies>
    </module>

?? [[   puis add provider dans standalone.xml

    <subsystem xmlns="urn:jboss:domain:keycloak-server:1.1">
        <web-context>auth</web-context>
        <providers>
            <provider>module:org.keycloak.examples.event-sysout</provider>
        </providers>
        ...

 ]] ??