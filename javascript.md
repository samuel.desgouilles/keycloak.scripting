Practical nashorn
===
https://developer.oracle.com/databases/nashorn-javascript-part1.html
https://docs.oracle.com/javase/8/docs/technotes/guides/scripting/nashorn/shell.html

jjs = lance class java - repl for js in java
puis jrunscript = launcher for jsr223 programs ( script)

jrunscript -q
Language ECMAScript ECMA - 262 Edition 5.1 implementation "Oracle Nashorn" 11.0.9.1
=> version 5.1 de js with some limited ECMAScript 6 features. (--language=es6 option)


module system
======
ecmascript 5 and earlier n ont pas de modulex
Nashorn itself doesn't incorporate any module system built in and relies only on the load() function to import dependencies. 

npm ecosystem is built upon this CommonJS format.
nodyn(https://github.com/nodyn/jvm-npm) <=== npm to nashorm? NPM module loading in Javascript runtimes on the JVM
CommonJS !

NODE_PATH env var
sinon 
    ('user.home') + '/.node_modules');
    ('user.home') + '/.node_libraries');

//uppercase.js
exports.uppercase = (str) => str.toUpperCase()
//use in any js
const uppercaseModule = require('uppercase.js')
uppercaseModule.uppercase('test')



Java from Nashorn 
======
java.util.HashMap, Packages.java.util.HashMap, and Java.type("java.util.HashMap") are three equivalent ways of accessing the Java hash map from Nashorn.




execution dans keycloak
===
si erreur pas d'onglet token et evaluate previent d'une erreur
ERROR [org.keycloak.protocol.oidc.mappers.ScriptBasedOIDCProtocolMapper] (default task-6) Error during execution of ProtocolMapper script: org.keycloak.scripting.ScriptExecutionException: Could not execute script 'token-mapper-script_attCalc' problem was:
ou pas ... pb load = onglet mais stack dans logs


class
===
ScriptProviderDescriptor.java


Removal nashorn : effective java 15 (15/09/2020 GA) : JEP 372: 	Remove the Nashorn JavaScript Engine
===

graalvm

https://medium.com/graalvm/nashorn-removal-graalvm-to-the-rescue-d4da3605b6cb
Unlike Nashorn, GraalVM JavaScript is able to execute Node.js applications. => access a npm mais pas en mode javascript engine seul?


===

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
source .bashrc
nvm install --lts <=install node et npm

npm init =< rep de travail
npm install
=> copie modules directory


===

ERRORS
    const = es6

    Cannot redefine property "exports" of [object Object]

    ES6 destructuring is not yet implemented
    const { TimeoutError } = require('../../errors')

    Cannot find module util <= fait partie de nodejs !
    existe sur npm mais ensuite pb avec process !!!
    => creation function inherit

    loading module ... : /var/home/sam/Work/eole.keycloak.scripts/scripts/js-modules/node_modules/assert-plus/assert.js
cannot create function from body java.lang.StackOverflowErrors

.....

===
GRAALVM

https://www.graalvm.org/reference-manual/js/

GraalVM treats Node.js as if it is a JVM language.( not yet for python NB GraalVM provides a Python 3.8 compliant runtime.)

GraalVM provides a fully-compliant ECMAScript 2020 

https://www.graalvm.org/reference-manual/js/NodeJSvsJavaScriptContext/#javascript-modules
CommonJS modules cannot be used directly from Java; to this end, any popular package bundlers (such as Parcel, Browserify, or Webpack) can be used.

GraalVM provides a JSR-223 compliant javax.script.ScriptEngine
=> use new js engine with jdk 8 +

https://github.com/keycloak/keycloak/blob/66dfa32cd569a7416de21b4dc04db212e8fccce5/services/src/main/java/org/keycloak/scripting/DefaultScriptingProviderFactory.java
=> class qui instancie le scriptenginemanager !

https://medium.com/graalvm/nashorn-removal-graalvm-to-the-rescue-d4da3605b6cb
https://medium.com/graalvm/graalvms-javascript-engine-on-jdk11-with-high-performance-3e79f968a819

===
!!!!
GraalVM.JS Scripting Provider extension for Keycloak
https://github.com/thomasdarimont/keycloak-graalvm-js-scripting-provider
!!!!!
===

r! Could not compile 'ldap-script' problem was: org.graalvm.polyglot.PolyglotException: SyntaxError: <eval>:5:0 Expected an operand but found import import ldap from 'ldapjs'; ^ <eval>:52:23 Expected ; but found findGenre const genreAmy = await findGenre(humanAmy); ^ <eval>:53:26 Expected ; but found findGenre const genreRodrig = await findGenre(robotRodrig); ^ 
=> utilisation de require

npm avec prefix ? vs package ?


Expected an operand but found import  => mjs 

https://stackoverflow.com/questions/59161450/graal-js-importing-module-causes-org-graalvm-polyglot-polyglotexception-syntaxe

https://github.com/oracle/graaljs/blob/master/docs/user/NodeJSVSJavaScriptContext.md#ecmascript-modules-ecm
MAScript modules can be loaded in a Context simply by evaluating the module sources. Currently, GraalVM JavaScript loads ECMAScript modules based on their file extension. Therefore, any ECMAScript module must have file name extension .mjs. This might change in future versions of GraalVM JavaScript.

https://www.graalvm.org/reference-manual/js/NodeJSvsJavaScriptContext/


cannot read from file => allow.io = false par defaut dans le scriptengine ?
instantiate on java.util.Scanner failed due to: Multiple applicable overloads found for method name java.util.Scanner
(candidates: [Method[public java.util.Scanner(java.lang.String)], Method[public java.util.Scanner(java.io.File) throws java.io.FileNotFoundException]], arguments: [JavaObject[/opt/jboss/keycloak/js-attr/js-modules/mon-module.js (java.io.File)] (HostObject)])


===> todo https://www.graalvm.org/reference-manual/js/ScriptEngine/  use Context ??
nop a priori binding ne change rie  et demarrer avec compt nashorn a true qui met toute les options a true !!

===
grall-js  /  graal-nodejs
