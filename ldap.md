<!-- https://github.com/rroemhild/docker-test-openldap -->

docker run -d -p 10389:10389 \
--name ldap \
docker.io/rroemhild/test-openldap

===

(openldap-clients)

ldapsearch -x -H ldap://127.0.0.1:10389 -b "cn=Amy Wong+sn=Kroker,ou=people,dc=planetexpress,dc=com" description
    => human
ldapsearch -x -H ldap://127.0.0.1:10389 -b "cn=Bender Bending Rodríguez,ou=people,dc=planetexpress,dc=com" description
    => robot

===

http://ldapjs.org/client.html#search
