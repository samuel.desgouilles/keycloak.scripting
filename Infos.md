3 type de scripting:
	- authenticators ( logique pour lauthenification )
	- policies
	- mappers ( openid protocol mappers )

Mise en oeuvre du scripting mappers

activer la possibilite de scripting
===

https://www.keycloak.org/docs/latest/server_installation/#profiles

lancer le serveur avec une variable système
	-Dkeycloak.profile.feature.scripts=enbled

pour que l'activation soit permanente
	creer fichier profile.properties dans le repertoire de config
	ajouter la ligne
		feature.scripts=enabled

attention different entre mode standalone et domain
	doit être creer dans le repertoire auquel fait reference la varaible 
		jboss.server.config.dir

attention si la variable système est positionnée, elle est prioritaire


===
https://www.keycloak.org/docs/latest/server_development/#_script_providers

si les entrees n'apparaissent pas dans la drop down list
  => verifier le jar et la presence du META INF ( a la racine du jar ) 

check demarrage
===

dans les logs de demarrages
  en debug : 
    configured system properties:
	...
    jboss.server.base.dir = /opt/jboss/keycloak/standalone
>>> jboss.server.config.dir = /opt/jboss/keycloak/standalone/configuration
    jboss.server.data.dir = /opt/jboss/keycloak/standalone/data
	...
INFO  [org.jboss.as.server.deployment.scanner] (MSC service thread 1-8) WFLYDS0013: Started FileSystemDeploymentService for directory /opt/jboss/keycloak/standalone/deployments
INFO  [org.jboss.as.server.deployment] (MSC service thread 1-5) WFLYSRV0027: Starting deployment of "MesScripts.jar" (runtime-name: "MesScripts.jar")
INFO  [org.jboss.as.server.deployment] (MSC service thread 1-6) WFLYSRV0027: Starting deployment of "keycloak-server.war" (runtime-name: "keycloak-server.war")
       ...
INFO  [org.keycloak.subsystem.server.extension.KeycloakProviderDeploymentProcessor] (MSC service thread 1-6) Deploying Keycloak provider: MesScripts.jar
	...
INFO  [org.keycloak.services] (ServerService Thread Pool -- 65) KC-SERVICES0001: Loading config from standalone.xml or domain.xml
INFO  [org.keycloak.common.Profile] (ServerService Thread Pool -- 65) Preview feature enabled: scripts  <<<<
	...
	...

  started and listening

=> voir info dans server info dans la console management!
+ onglet provider / protocol-mapper 


Quelques classes java 
===

  - org.keycloak.protocol.oidc.mappers.ScriptBasedOIDCProtocolMapper
  - org.keycloak.common.Profile


Coder javascript
===
/**
 * Available variables: 
 * user - the current user (UserModel)
 * realm - the current realm (RealmModel)
 * token - the current token (TokenModel)
 * userSession - the current userSession (UserSessionModel)
 * keycloakSession - the current keycloakSession (KeycloakSessionModel)
 */
user: org.keycloak.models.cache.infinispan.UserAdapter@fef1d307
realm: ACA@0000fc5f
token: org.keycloak.representations.AccessToken@45415f0d
userSession: org.keycloak.models.sessions.infinispan.UserSessionAdapter@95840f03
keycloakSession: org.keycloak.services.DefaultKeycloakSession@535e6e35

token.getOtherClaims().put("mon-attribut-calcule", monmodule());